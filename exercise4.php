<?php 
    // Exercise 4: PHP 
    // Task Deliverables:
    // 1. Create a PHP file.
    // 2. Inside of it, create a class named "API". 
    // 3. Inside of the class, create a 3 function with parameters that execute printing the parameters you pass.
    // 4. Outside of Class, use the class and call the function you created. 
    // 5. In the 1st function, put your full name as the parameter of it.
    // 6. In the 2nd Function, put an Array Value of your hobbies as the parameter of it.
    // 7. In the 3rd Function, put an Object Value of your Age, email address and Birthday as the 
?>

<?php 
    
    class API {
        public function myFullName($fullName){
            echo "Full Name: $fullName <br>";
        }

        public function myHobbies($hobbies){
            echo "Hobbies:<br>";
            foreach ($hobbies as $hobby) {
                echo "&nbsp;&nbsp;&nbsp;$hobby<br>";
            }
        }

        public function myPersonalInfo($age, $email, $birthday){
            echo "Age: $age <br>";
            echo "Email: $email <br>";
            echo "Birthday: $birthday <br>";
        }
    }

    $API = new API();

    $API->myFullName("Lester Allen P. Cabucos");

    $listHobbies = ["Playing Online Games", "Reading Manhwa", "Watching tutorials"];    
    $API->myHobbies($listHobbies);

    $API->myPersonalInfo(23, "l.a.cabucos@gmail.com", "December 11, 2000");
?>