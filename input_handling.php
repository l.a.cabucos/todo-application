<?php 
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $name = $_POST['name'];

        // TODO: output personalized greeting message
        if ($name == 'John'){
            echo "Hello, $name! Welcome to our website.";
        } else if($name == 'Matthew'){
            echo "Hello, $name! Welcome to our website.";
        } else if ($name == 'Mark') {
            echo " Hello, $name! Welcome to our website.";
        } else if ($name == 'Luke') {
            echo " Hello, $name! Welcome to our website.";
        } else {
            echo "Input should be empty. Enter your name.";
        }
    }
?>

<form method="POST">
    <label for="name">Enter your name:</label>
    <input type="text" id="name" name="name">
    <button type="submit">Submit</button>
</form>